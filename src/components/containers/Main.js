import { Switch, Route } from 'react-router-dom'
import EmployeesListScreen from "../employees_list/EmployeesListScreen";
import EmployeesDataScreen from "../employees_data/EmployeesDataScreen";
import LoginScreen from "../login/LoginScreen";
import React from "react";

class Main extends React.Component {

    render() {
        return (
            <main>
                <Switch>
                    <Route exact path='/' component={LoginScreen}/>
                    <Route path='/data' component={EmployeesDataScreen}/>
                    <Route path='/employees' component={EmployeesListScreen}/>
                </Switch>
            </main>
        );
    }
}

export default Main;