import { Link } from 'react-router-dom'
import React from "react";

class Header extends React.Component {

    render() {
        return (
            <header>
                <nav>
                    <ul className="MenuUl">
                        <li className="MenuLi"><Link to='/'>Войти</Link></li>
                        <li className="MenuLi"><Link to='/data'>Данные работников</Link></li>
                        <li className="MenuLi"><Link to='/employees'>Список работников</Link></li>
                    </ul>
                </nav>
            </header>
        );
    }
}

export default Header;