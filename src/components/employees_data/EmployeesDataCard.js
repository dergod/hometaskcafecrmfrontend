import React from "react";
import EmployeesDataTable from "./EmployeesDataTable";

const data = [
    {name: 'Иван', surname: 'Иванов', sales: '2', tips: '2', ssg: '2'},
    {name: 'Петр', surname: 'Петров', sales: '5', tips: '2', ssg: '2'},
    {name: 'Василий', surname: 'Васильев', sales: '4', tips: '2', ssg: '2'},
    {name: 'Майкл', surname: 'Майклов', sales: '2', tips: '4', ssg: '6'},
    {name: 'Булат', surname: 'Булатов', sales: '5', tips: '6', ssg: '6'},
    {name: 'Роман', surname: 'Романов', sales: '8', tips: '6', ssg: '2'},
    {name: 'Ниф', surname: 'Нифов', sales: '3', tips: '3', ssg: '7'},
    {name: 'Наф', surname: 'Нафов', sales: '7', tips: '6', ssg: '8'},
    {name: 'Нуф', surname: 'Нуфов', sales: '5', tips: '5', ssg: '4'}
];

class EmployeesDataCard extends React.Component {

    render() {
        return (
            <div className="EmployeesDataCard">
                <EmployeesDataTable data={data}/>
                <button className="AnyButton">Отправить данные</button>
            </div>
        );
    }
}

export default EmployeesDataCard;