import React from "react";
import Calendar from 'react-calendar'
import EmployeesDataCard from "./EmployeesDataCard";

/**
 * Компонент экрана для ввода данных
 */
class EmployeesDataScreen extends React.Component {

    render() {
        return (
            <div className="AnyScreen">
                <EmployeesDataCard/>
                <Calendar className="Calendar" />
            </div>
        );
    }
}

export default EmployeesDataScreen;