import React, { Component } from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import '../../../node_modules/react-bootstrap-table/css/react-bootstrap-table.css'

class EmployeesDataTable extends Component {

    render() {
        const cellEditProp = {
            mode: 'click'
        };

        return (
            <div>
                <BootstrapTable data={this.props.data}
                                version='4'
                                cellEdit={cellEditProp}
                                className="EmployeesDataTable">
                    <TableHeaderColumn isKey
                                       dataField='name'
                                       dataAlign='left'
                                       headerAlign="left"
                                       width="200px">
                        Имя
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField='surname'
                                       dataAlign='left'
                                       headerAlign="left"
                                       width="200px">
                        Фамилия
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField='sales'
                                       dataAlign='left'
                                       headerAlign="left"
                                       width="200px">
                        Продажи
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField='tips'
                                       dataAlign='left'
                                       headerAlign="left"
                                       width="200px">
                        Чаевые
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField='ssg'
                                       dataAlign='left'
                                       headerAlign="left"
                                       width="200px">
                        ССГ
                    </TableHeaderColumn>
                </BootstrapTable>
            </div>
        );
    }
}

export default EmployeesDataTable;