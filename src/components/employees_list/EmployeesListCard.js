import React from "react";

class EmployeesListCard extends React.Component {

    render() {
        return (
            <div className="EmployeesListCard">
                <div className="AnyText">Иван Иванов</div>
                <div className="AnyText">Петр Петров</div>
                <div className="AnyText">Василий Васильев</div>
                <div className="AnyText">Майкл Майклов</div>
                <div className="AnyText">Виктор Викторов</div>
                <div className="AnyText">Роман Романов</div>
                <div className="AnyText">Булат Булатов</div>
                <div className="AnyText">Ниф Нифов</div>
                <div className="AnyText">Наф Нафов</div>
                <div className="AnyText">Нуф Нуфов</div>
            </div>
        );
    }
}

export default EmployeesListCard;