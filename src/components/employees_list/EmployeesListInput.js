import React from "react";

class EmployeesListInput extends React.Component {

    render() {
        return (
            <div className="EmployeesListInput">
                <div className="AnyText">Имя</div>
                <input type="input" className="AnyInput"/>
                <div className="AnyText">Фамилия</div>
                <input type="input" className="AnyInput"/>
                <div className="AnyText">Должность</div>
                <input type="input" className="AnyInput"/>
                <button className="AnyButton">Добавить</button>
            </div>
        );
    }
}

export default EmployeesListInput;