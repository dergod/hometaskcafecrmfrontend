import React from "react";
import EmployeesListCard from "./EmployeesListCard";
import EmployeesListInput from "./EmployeesListInput";

/**
 * Компонент экрана для списка работников
 */
class EmployeesListScreen extends React.Component {

    render() {
        return (
            <div className="AnyScreen">
                <EmployeesListCard/>
                <EmployeesListInput/>
            </div>
        );
    }
}

export default EmployeesListScreen;