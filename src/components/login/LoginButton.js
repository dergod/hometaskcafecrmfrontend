import React from "react";
import {Route} from "react-router-dom";

class LoginButton extends React.Component {

    processAuth(history) {
        console.log(this.props.password + " log");
        if (String(this.props.login).valueOf() === "admin") {
            history.push('/data');
        }
    };

    render() {
        return (
            <Route render={({ history }) => (
                <button className="AnyButton" onClick={this.processAuth.bind(this, history)}>OK</button>
            )} />
        )};
}

export default LoginButton;