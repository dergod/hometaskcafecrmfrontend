import React from "react";
import LoginButton from "./LoginButton";

class LoginCard extends React.Component {

    constructor(props) {
        super(props);
        this.state = {login: "", password: ""};
    }

    changeLogin = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    changePassword = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    render() {
        return (
            <div className="LoginCard">
                <div className="AnyText">Логин</div>
                <input type="input"
                       name="login"
                       className="AnyInput"
                       value={this.state.login}
                       onChange={this.changeLogin}/>
                <div className="AnyText">Пароль</div>
                <input type="input"
                       name="password"
                       className="AnyInput"
                       value={this.state.password}
                       onChange={this.changePassword}/>
                <LoginButton login={this.state.login} name={this.state.password} />
            </div>
        );
    }
}

export default LoginCard;