import React from "react";
import LoginCard from "./LoginCard";

class LoginScreen extends React.Component {

    render() {
        return (
            <div className="AnyScreen">
                <LoginCard/>
            </div>
        );
    }
}

export default LoginScreen;